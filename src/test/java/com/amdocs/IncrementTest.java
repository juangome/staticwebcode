package com.amdocs;
import static org.junit.Assert.*;
import org.junit.Test;

public class IncrementTest {
	
	@Test
	public void testIncrement1() throws Exception {
	int k= new Increment().decreasecounter(1);
	assertEquals("Testing 1",1, k);
	}

        @Test
        public void testIncrement0() throws Exception {
        int k= new Increment().decreasecounter(0);
        assertEquals("Testing 0",1, k);
	}
        @Test
        public void testIncrement10() throws Exception {
        int k= new Increment().decreasecounter(10);
        assertEquals("Testing 10",1, k);		
	}
}	
